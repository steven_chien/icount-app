##Readme## 

Assignment 1 of HK PolyU COMP305, fall 2012

##Introduction##

This is the Assignment 1 for COMP305 Data Structure and Algorithm I of Hong Kong Polytechnic University, fall 2012. 
The program read a list of words from a file and store them into a link list and count their use in the file then list the words in order of their frequency of use in the file.

##License##

Copyright (C) 2012 Steven Chien

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.